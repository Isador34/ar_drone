#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#include <RT_ARDrone/RT_ARDrone.h>

#include "glwindow.h"


typedef struct {
    float h;       // angle in degrees
    float s;       // percent
    float v;       // percent
} hsv;


char* getColorName(hsv hs){
	float h = hs.h;
	float s = hs.s;
	float v = hs.v;
	
	//~ if (h <20 )//&& s < 20 && v <20)
		//~ return "BLACK";
	//~ if (h <20 )//&& s < 20 && v >90)
		//~ return "WHITE";
	/*if (h < 30 && s<60)//&& s > 70 && v >70)
		return "RED";
	else if (h > 140 && h < 160 && s>20)//&& h <150 && s > 50 && v > 50)
		return "GREEN";
	else if (h > 180 && h < 260 && s>40)//&& h <260 && s > 50 && v >50)//
		return "BLUE";
	else
		return "?";*/
		
	if(h<30 && s>50)
		return "RED";
	else if(h > 90 && h < 120 && s > 20)
		return "GREEN";
	else if(h > 230 && h < 250 && s > 40)
		return "BLUE";
	else
		return "";
	
}


int getPixelByCoord(int x, int y)
{
	return ( y * 640+x )*3;
}

int* getCoordByPixel(int p)
{
	int* coor;
	coor[0] = fmod(p, 640)/3;
	coor[1] = ((p/3)-coor[0])/640;
	return coor;
	
}

float max(float a, float b, float c){
	
	float max1 = ((a+b) + fabs(a-b))/2;
	return ((max1 + c) + fabs(max1-c))/2;
}

float min(float a, float b, float c){
	
	float min1 = ((a+b) - fabs(a-b))/2;
	return ((min1 + c) - fabs(min1-c))/2;
}

hsv toHsv(int r, int g, int b){
	
	float cMax = max(r, g, b);
	float cMin = min(r, g, b);
	
	float d = cMax - cMin;
	
	float h=0;
	if(d != 0)
	{
		if(cMax == r)
		{
			h = fmod((60 * (g-b)/d + 360), 360);
		}
		else if(cMax == g)
		{
			h = 60 * (b-r)/d + 120;
		}
		else if(cMax == b)
		{
			h = 60 * (r-g)/d + 240;
		}
	}
	
	float s = 0;
	if(cMax != 0)
		s = (cMin/cMax) * 100;
	
	float v = cMax;
	
	hsv hs;
	hs.h = h;
	hs.s = s;
	hs.v = v;
	
	return hs;
}

/*
hsv toHsv(int r, int g, int b){		
	float rr = (float) (r / 255.0);
	float gg = (float) (g / 255.0);
	float bb = (float) (b / 255.0);
	
	float cMax = max(rr, gg, bb); 
		
	float cMin = min(rr, gg, bb); 
		
		
	float h = -1;
		
	float d = cMax - cMin;
		
	//printf("r %lf g %lf b %lf max %lf min %lf\n", rr, gg, bb, cMax, cMin);
		
				
	if(d == 0){
		h = 0;
	}
	else if(cMax == rr){
		h = 60 * (  fmod( ((gg-bb)/d), 6.0 ) );
	}	
	else if(cMax == gg){
		h = 60 * ( ((bb-rr)/d) +2 );
	}
	else if(cMax == bb){
		h = 60 * ( ((rr-gg)/d)+4 );
	}
			
	float s=0;
	if(cMax != 0)
		s = d/cMax * 100.0;
	
	//printf("%lf %lf %lf\n", d, cMax, cMin);
	
	float v = cMax * 100;
	
	hsv hs;
	hs.h = h;
	hs.s = s;
	hs.v = v;
	
	return hs;
}*/


int main() {

	GLWindow*   win ;
	ARDrone*    bob ;
	RGB24Image* img ;
	NavData start;
	NavData current;
	NavData last;

	img = RGB24Image_new(640,360) ;
	bob = ARDrone_new( "192.168.1.1" ) ;
	win = GLWindow_new ( "glview", 640, 360, 0 ) ;

	ARDrone_connect( bob ) ;

	ARDrone_trim ( bob ) ;

	sleep(2) ;
	
	printf("start\n");

	float phi[15];
	float psi[15];
	
	
	int j = 0;
	printf("get\n");
	while(j < 15)
	{
		printf("%d\n", j);
		ARDrone_get_navdata( bob, &start );
		printf("test1\n");
		phi[j] = start.phi;
		printf("test2\n");
		psi[j] = start.psi;
		printf("test3\n");
		j++;
	}
	printf("fin get\n");

	j = 0;
	float phiMax = phi[0];
	float phiMin = phi[0];
	
	float psiMax = psi[0];
	float psiMin = psi[0];
	printf("select\n");

	while(j < 15)
	{
		if(phi[j] > phiMax)
			phiMax = phi[j];
		if(phi[j] < phiMin)
			phiMin = phi[j];
		if(psi[j] > psiMax)
			psiMax = psi[j];
		if(psi[j] < psiMin)
			psiMin = psi[j];
		j++;
	}
	
	float finPhi = 0;
	float finPsi = 0;
	
	j=0;
	int kPhi=0;
	int kPsi=0;
	printf("final\n");

	while(j <15)
	{
		if(phi[j] != phiMin && phi[j] != phiMax){
			kPhi++;
			finPhi += phi[j];
		}
		
		if(psi[j] != psiMin && psi[j] != psiMax){
			kPsi++;
			finPsi += phi[j];
		}
		j++;
	}
	
	finPhi /= kPhi;
	finPsi /= kPsi;
	
	printf("decolage\n");
	//usleep(8000000) ;
	ARDrone_takeoff(bob);
	usleep(4000000);
	printf("stabilisation\n");
	ARDrone_move(bob, 1, 0, 0, 0, 0);
	usleep(3000000);

	ARDrone_zap_camera ( bob, 1);

	printf("avance\n");

	ARDrone_move(bob, 1, 0, -0.05, 0, 0);

	int i=0;
	
	float fphi = 0, fpsi = 0;

	int nRed = 0;
	int nGreen = 0;
	int nBlue = 0;
	
	int totalGreen = 0;

	while (1) {
		
		nRed = 0;
		nGreen = 0;
		nBlue = 0;
		
		ARDrone_get_navdata( bob, &current );

		//~ printf("sPHI %lf sPSI %lf cPHI %lf cPSI %lf bat %d\n", fabs(start.phi), fabs(start.psi), fabs(current.phi), fabs(current.psi), current.bat);

	//	ARDrone_move(bob, 1, fphi*4, -0.05, fpsi*4, 0);
	if(totalGreen == 0)
		ARDrone_move(bob, 1, -0.0089, -0.08, 0.00, 0);
	if(totalGreen == 1)
		ARDrone_move(bob, 1, -0.05, -0.08, 0.00, 0);
	if(totalGreen == 2)
		ARDrone_move(bob, 1, -0.017, -0.08, 0.00, 0);
		
		/*	if (fabs(current.phi) < fabs(finPhi))
				ARDrone_move(bob, 1, 1, -0.05, 0, 0);
            else if (fabs(current.phi) > fabs(finPhi))
				ARDrone_move(bob, 1, -1, -0.05, 0, 0);
				
			if (fabs(current.psi) < fabs(finPsi))
				ARDrone_move(bob, 1, 0, -0.05, 1, 0);
			else if (fabs(current.psi) > fabs(finPsi))
				ARDrone_move(bob, 1, 0, -0.05, -1, 0);*/

		ARDrone_get_RGB24Image ( bob, img ) ;

		GLWindow_draw_rgb(win,img->pixels );

		GLWindow_swap_buffers(win);

		XEvent event;

		while ( GLWindow_next_event( win, &event ) ) {

			GLWindow_process_events( win, event );
		}
		
		while(i< 640)//diagonale haut gauche vers bas droit
		{
			
			int y = ((9.0 * i)/16.0);
			int v = getPixelByCoord(i,y);
			
			int r = img->pixels[v];
			int g = img->pixels[v+1];
			int b = img->pixels[v+2];
			
			hsv h = toHsv(r, g, b);
			
			//printf("%d %d %d\n", r, g, b);
			char* c = getColorName(h);
			//~ printf("%s ", c);
			//~ printf("%lf %lf %lf color:%s\n", h.h, h.s, h.v, c);
			
			if(c == "RED")
				nRed++;
			else if(c == "GREEN")
				nGreen++;
			else if(c == "BLUE")
				nBlue++;
			
			i++;
		}
		//printf("\n\n");
		i=640;
		while(i >= 0)//diagonale haut droit vers bas gauche
		{
			
			int y = ((9.0 * i)/16.0);
			int v = getPixelByCoord(-i,y);
			
			int r = img->pixels[v];
			int g = img->pixels[v+1];
			int b = img->pixels[v+2];
			
			hsv h = toHsv(r, g, b);
			
			//printf("%d %d %d\n", r, g, b);
			char* c = getColorName(h);
			//~ printf("%s ", c);
			//~ printf("%lf %lf %lf color:%s\n", h.h, h.s, h.v, c);
			
			if(c == "RED")
				nRed++;
			else if(c == "GREEN")
				nGreen++;
			else if(c == "BLUE")
				nBlue++;
			
			i--;
		}
		//printf("\n\n");
		
		i=0;
		
		while(i<640)//horizontal
		{
			int v = getPixelByCoord(-i,180);
			int r = img->pixels[v];
			int g = img->pixels[v+1];
			int b = img->pixels[v+2];
			
			hsv h = toHsv(r, g, b);
			
			//printf("%d %d %d\n", r, g, b);
			char* c = getColorName(h);
			//~ printf("%s ", c);
			//~ printf("%lf %lf %lf color:%s\n", h.h, h.s, h.v, c);
			
			if(c == "RED")
				nRed++;
			else if(c == "GREEN")
				nGreen++;
			else if(c == "BLUE")
				nBlue++;
			
			i++;
		}
		//printf("\n\n");
		
		i=0;
		
		while(i<360)
		{
			int v = getPixelByCoord(-320, i);//vertical
			int r = img->pixels[v];
			int g = img->pixels[v+1];
			int b = img->pixels[v+2];
			
			hsv h = toHsv(r, g, b);
			
			//printf("%d %d %d\n", r, g, b);
			char* c = getColorName(h);
			//~ printf("%s ", c);
			//~ printf("%lf %lf %lf color:%s\n", h.h, h.s, h.v, c);
			
			if(c == "RED")
				nRed++;
			else if(c == "GREEN")
				nGreen++;
			else if(c == "BLUE")
				nBlue++;
			
			i++;
		}
		//~ printf("\n\n");
		
		i=0;
		
		//~ printf("red %d, blue %d, green %d\n", nRed, nBlue, nGreen);

		int seuil = 670;

		if(nGreen > seuil)
		{
			totalGreen++;
			switch(totalGreen)
			{
				case 1:
					printf("CHECKPOINT 1\n");
					ARDrone_move(bob, 0, 0, 0, 0, 0);
					usleep(2000000);
					ARDrone_move(bob, 1, 0, -0.05, 0, 0);
					usleep(1000000);
					break;
				case 2:
					printf("CHECKPOINT 2\n");
					ARDrone_move(bob, 0, 0, 0, 0, 0);
					usleep(2000000);
					ARDrone_move(bob, 1, 0, -0.05, 0, 0);
					usleep(1000000);
					break;
				case 3:
					printf("CHECKPOINT 3\n");
					ARDrone_move(bob, 0, 0, 0, 0, 0);
					usleep(2000000);
					ARDrone_move(bob, 1, 0, -0.05, 0, 0);
					usleep(1000000);
					break;
				case 4:
					printf("CHECKPOINT 4\n");
					ARDrone_move(bob, 0, 0, 0, 0, 0);
					usleep(800000);
					ARDrone_move(bob, 0, 0, 0, 1, 0);				
					usleep(2000000);
					ARDrone_move(bob, 0, 0, -0.05, 0, 0);
					usleep(1000000);
					break;
				case 5:
					printf("CHECKPOINT 5\n");
					ARDrone_move(bob, 0, 0, 0, 0, 0);
					usleep(2000000);
					ARDrone_land(bob);
					usleep(4000000);
					ARDrone_takeoff(bob);
					usleep(8000000);
					ARDrone_move(bob, 0, 0, 0, 1, 0);
					usleep(1800000);
					ARDrone_move(bob, 0, 0, -0.05, 0, 0);
					usleep(1000000);
					break;
				case 6:
					printf("CHECKPOINT 6\n");
					usleep(1000000);
					break;
				case 7:
					printf("CHECKPOINT 7\n");
					ARDrone_move(bob, 0, 0, 0, 0, 0);
					usleep(2000000);
					ARDrone_land(bob);
					usleep(4000000);
					ARDrone_reset_defaults(bob);
					ARDrone_free(bob);
					break;
			}
			//~ ARDrone_move(bob, 0, 0, 0, 0, 0);
			//~ usleep(4000000);
			//~ ARDrone_land(bob);
		}

		ARDrone_get_navdata(bob, &last);
		
		fphi = current.phi - last.phi;
		fpsi = current.psi - last.psi;
		//~ printf("phi %lf psi %lf\n", fphi, fpsi);

	}
	usleep(2000000);
//	ARDrone_land(bob);
//	usleep(4000000);
	ARDrone_reset_defaults( bob ) ;
	ARDrone_free(bob);
	return 0 ;

}
