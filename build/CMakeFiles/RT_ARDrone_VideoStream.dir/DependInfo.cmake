# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/isador/Bureau/RT_ARDrone/tools/drone_videostream/src/glwindow.c" "/home/isador/Bureau/RT_ARDrone/tools/drone_videostream/build/CMakeFiles/RT_ARDrone_VideoStream.dir/src/glwindow.c.o"
  "/home/isador/Bureau/RT_ARDrone/tools/drone_videostream/src/main.c" "/home/isador/Bureau/RT_ARDrone/tools/drone_videostream/build/CMakeFiles/RT_ARDrone_VideoStream.dir/src/main.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../include"
  "../src"
  "."
  "/usr/local/include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
